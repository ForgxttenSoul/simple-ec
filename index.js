const sha256 = require("sha256");
const crypto = require("crypto");
const ecc = require("eccrypto");
const secp256k1 = require("secp256k1");
const ecdh = require("ecdh");
const curve_secp256k1 = ecdh.getCurve("secp256k1");
const base58 = require("bs58");
const sha512 = (input) => {
    return (buffer2hex(crypto.createHash("sha512").update(input).digest()));
};
const buffer2hex = (input) => {
    return (input.toString("hex"));
};
const hex2buffer = (input) => {
    return (Buffer.from(input, "hex"));
};
const str2base = (input) => {
    return (Buffer.from(input).toString("base64"));
};
const base2str = (input) => {
    return (Buffer.from(input, "base64").toString("ascii"));
};
const pad32 = (messageBuffer) => {
    if (messageBuffer.length < 32) {
        const buffer = hex2buffer("".padstart(32, "0"));
        buffer.fill(0);
        messageBuffer.copy(buffer, 32 - messageBuffer.length);
        return (buffer);
    }
    return (messageBuffer);
};
const aes256CbcEncrypt = (iv, key, plaintext) => {
    const cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
    const firstChunk = cipher.update(plaintext);
    const secondChunk = cipher.final();
    return (Buffer.concat([firstChunk, secondChunk]));
};
const aes256CbcDecrypt = (iv, key, ciphertext) => {
    const cipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
    const firstChunk = cipher.update(ciphertext);
    const secondChunk = cipher.final();
    return (Buffer.concat([firstChunk, secondChunk]));
};
const hmacSha256 = (shaKey, message) => {
    return (crypto.createHmac("sha256", shaKey).update(message).digest());
};
const ecPrivateKey = (input = false) => {
    if (input) return (sha256(input));
    return (buffer2hex(crypto.randomBytes(32)));
};
const ecPublicKey = (privateKey) => {
    return (buffer2hex(ecc.getPublic(hex2buffer(privateKey))));
};
const ecSign = (privateKey, message) => {
    privateKey = hex2buffer(privateKey);
    message = hex2buffer(sha256(message));
    message = pad32(message);
    const signature = secp256k1.sign(message, privateKey).signature;
    return (buffer2hex(secp256k1.signatureExport(signature)));
};
const ecVerify = (publicKey, message, signature) => {
    publicKey = hex2buffer(publicKey);
    message = hex2buffer(sha256(message));
    message = pad32(message);
    signature = hex2buffer(signature);
    signature = secp256k1.signatureImport(signature);
    return (secp256k1.verify(message, signature, publicKey));
};
const ecDerive = (privateKey, publicKey) => {
    publicKey = publicKey.startsWith("04") ? publicKey.substr(2, 128) : publicKey;
    privateKey = ecdh.PrivateKey.fromBuffer(curve_secp256k1, hex2buffer(privateKey));
    publicKey = ecdh.PublicKey.fromBuffer(curve_secp256k1, hex2buffer(publicKey));
    return (buffer2hex(privateKey.deriveSharedSecret(publicKey)));
};
const ecEncrypt = (publicKey, message, privateKey = false, iv = false) => {
    if (!privateKey) privateKey = ecPrivateKey();
    const ephem = {
        privateKey
    };
    ephem.publicKey = ecPublicKey(ephem.privateKey);
    const shared = {
        privateKey: ecDerive(ephem.privateKey, publicKey)
    };
    shared.publicKey = ecPublicKey(shared.privateKey);
    const hash = hex2buffer(sha512(hex2buffer(shared.privateKey)));
    iv = iv ? iv : crypto.randomBytes(16);
    const encKey = hash.slice(0, 32);
    const macKey = hash.slice(32);
    const encMessage = aes256CbcEncrypt(iv, encKey, message);
    const dataToMac = Buffer.concat([iv, hex2buffer(ephem.publicKey), encMessage]);
    const mac = hmacSha256(macKey, dataToMac);
    const output = {
        iv,
        publicKey: ephem.publicKey,
        encMessage,
        mac
    };
    let base64 = str2base(JSON.stringify(output));
    return (base64);
};
const ecDecrypt = (privateKey, base64) => {
    let {
        encMessage,
        publicKey,
        iv,
        mac
    } = base64Data = JSON.parse(base2str(base64));
    iv = Buffer.from(iv);
    encMessage = Buffer.from(encMessage);
    mac = Buffer.from(mac);
    let shared = {
        privateKey: ecDerive(privateKey, publicKey)
    };
    shared.publicKey = ecPublicKey(shared.privateKey);
    const hash = hex2buffer(sha512(hex2buffer(shared.privateKey)));
    const encKey = hash.slice(0, 32);
    const macKey = hash.slice(32);
    const dataToMac = Buffer.concat([
        iv,
        hex2buffer(publicKey),
        Buffer.from(encMessage)
    ]);
    const realMac = hmacSha256(macKey, dataToMac);
    if (buffer2hex(realMac) !== buffer2hex(mac)) return (false);
    const messageBuffer = aes256CbcDecrypt(iv, encKey, encMessage);
    return (messageBuffer.toString("utf8"));
};

let $;
$ = module.exports = {
    ecc,
    crypto,
    sha256,
    sha512,
    ecdh,
    secp256k1,
    // .str2b64(text) -> base64Data
    str2b64: str2base,
    // .b642str(base64Data) -> text
    b642str: base2str,
    // .base58.encode(text) -> base58Buffer
    // .base58.decode(base58Buffer) -> text buffer
    base58,
    base64: {
        encode: (input) => input.toString("base64"), // -> base64 buffer
        decode: (input) => Buffer.from(input, "base64") // -> text buffer
    },
    hexTob58: (input) => base58.encode(hex2buffer(input)), // hex -> base58
    b58ToHex: (input) => buffer2hex(base58.decode(input)), // base58 -> hex
    // .privateKey(string/null) -> privateKey
    privateKey: ecPrivateKey,
    // .publicKey(privateKey) -> publicKey
    publicKey: ecPublicKey,
    // .sign(privateKey, message) -> signature
    sign: ecSign,
    // .verify(publicKey, message, signature) -> boolean
    verify: ecVerify,
    // .derive(privateKey, publicKey) -> sharedKey
    derive: ecDerive,
    // .encrypt(publicKey, message) -> encMessage
    encrypt: ecEncrypt,
    // .decrypt(privateKey, encMessage) -> message
    decrypt: ecDecrypt,
    // .sign64(privateKey, message) -> base64Signature
    sign64: (privateKey, message) => {
        return (
            str2base(
                JSON.stringify({
                    message,
                    signature: ecSign(privateKey, message)
                })
            )
        );
    },
    // .verify64(publicKey, base64Signature) -> boolean
    verify64: (publicKey, signatureb64) => {
        let sig = base2str(signatureb64)
        sig = JSON.parse(sig);
        console.log(sig);
        return (ecVerify(publicKey, sig.message, sig.signature));
    }
};