# simple-ec

    module.exports = {
        // .str2b64(text) -> base64Data
        str2b64: str2base,
        // .b642str(base64Data) -> text
        b642str: base2str,
        // .base58.encode(text) -> base58Buffer
        // .base58.decode(base58Buffer) -> text buffer
        base58,
        base64: {
            encode: (input) => input.toString("base64"), // -> base64 buffer
            decode: (input) => Buffer.from(input, "base64") // -> text buffer
        },
        hexTob58: (input) => base58.encode(hex2buffer(input)), // hex -> base58
        b58ToHex: (input) => buffer2hex(base58.decode(input)), // base58 -> hex
        // .privateKey(string/null) -> privateKey
        privateKey: ecPrivateKey,
        // .publicKey(privateKey) -> publicKey
        publicKey: ecPublicKey,
        // .sign(privateKey, message) -> signature
        sign: ecSign,
        // .verify(publicKey, message, signature) -> boolean
        verify: ecVerify,
        // .derive(privateKey, publicKey) -> sharedKey
        derive: ecDerive,
        // .encrypt(publicKey, message) -> encMessage
        encrypt: ecEncrypt,
        // .decrypt(privateKey, encMessage) -> message
        decrypt: ecDecrypt,
        // .sign64(privateKey, message) -> base64Signature
        sign64: (privateKey, message) => {
            return (
                str2base(
                    JSON.stringify({
                        message,
                        signature: ecSign(privateKey, message)
                    })
                )
            );
        },
        // .verify64(publicKey, base64Signature) -> boolean
        verify64: (publicKey, signatureb64) => {
            let sig = base2str(signatureb64)
            sig = JSON.parse(sig);
            console.log(sig);
            return (ecVerify(publicKey, sig.message, sig.signature));
        }
    }